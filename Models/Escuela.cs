using System;
using System.Collections.Generic;

namespace Curso_de_ASP.Net_Core.Models
{
    public class Escuela : ObjetoEscuelaBase
    {
        public int AnhoFundacion { get; set; }

        public string Pais { get; set; }
        public string Ciudad { get; set; }

        public string Direccion { get; set; }

        public TiposEscuela TipoEscuela { get; set; }
        public List<Curso> Cursos { get; set; }

        public Escuela()
        {

        }

        public Escuela(string nombre, int anho) => (Nombre, AnhoFundacion) = (nombre, anho);

        public Escuela(string nombre, int anho,
                       TiposEscuela tipo,
                       string pais = "", string ciudad = "") : base()
        {
            (Nombre, AnhoFundacion) = (nombre, anho);
            Pais = pais;
            Ciudad = ciudad;
        }

        public override string ToString()
        {
            return $"Nombre: \"{Nombre}\", Tipo: {TipoEscuela} {System.Environment.NewLine} Pais: {Pais}, Ciudad:{Ciudad}";
        }
    }
}
