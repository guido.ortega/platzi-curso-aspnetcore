using System;
using System.Collections.Generic;

namespace Curso_de_ASP.Net_Core.Models
{

    public class AlumnoPromedio
    {
        public float Promedio;
        public string Alumnoid;
        public string AlumnoNombre;
    }
}