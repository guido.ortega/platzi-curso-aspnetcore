using System;
using Microsoft.AspNetCore.Mvc;
using Curso_de_ASP.Net_Core.Models;

namespace Curso_de_ASP.Net_Core.Controllers
{
    public class EscuelaController : Controller
    {
        public IActionResult Index()
        {
            var escuela = new Escuela();
            escuela.AnhoFundacion = 2005;
            escuela.Nombre = "G-School";

            return View(escuela);
        }
    }
}